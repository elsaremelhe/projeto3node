//Imports
var mongodb = require('mongodb');
var objectID = mongodb.ObjectID;
var crypto = require('crypto');
var express = require('express');
var bodyParser = require('body-parser');
var fs = require('fs');
const { hash } = require('bcrypt');


//PSWD Utils

//FUNCS to random salt

var genRandomString = function (length) {
    return crypto.randomBytes(Math.ceil(length / 2)).toString('hex').slice(0, length);
};

var sha512 = function (pswd, salt) {
    var hash = crypto.createHmac('sha512', salt);
    hash.update(pswd);
    var value = hash.digest('hex');

    return {
        salt: salt,
        pswdHash: value
    };
};

function saltHashPswd(userPswd) {
    var salt = genRandomString(16); //Create 16 random characters
    var pswdData = sha512(userPswd, salt);
    return pswdData;
};

function checkHashPswd(userPswd, salt) {
    var pswdData = sha512(userPswd, salt);
    return pswdData;
}

//Generate random number
function between(min, max) {
    return Math.floor(
        Math.random() * (max - min) + min
    )
}


//Express service
var app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

//MongoDB Client
var mongoClient = mongodb.MongoClient;

//Connection URL
var url = 'mongodb://localhost:27017' //default port

mongoClient.connect(url, { useNewUrlParser: true }, function (err, client) {
    if (err) {
        console.log('Unable to connect to DB server. Error: ', err);
    }
    else { //Start WS ada

        //Register user
        app.post('/register', (request, response, next) => {
            var postData = request.body;

            var plaintPswd = postData.pswd;
            var hashData = saltHashPswd(plaintPswd);

            var pswd = hashData.pswdHash; //save pswd hash
            var salt = hashData.salt; //save salt

            var name = postData.name;
            var email = postData.email;
            var inviteCode = postData.inviteCode;

            var insertJson = {
                'email': email,
                'pswd': pswd,
                'salt': salt,
                'name': name,
                'XP': 0,
                'nível': 0
            };


            var db = client.db('ProjetoIII');


            db.collection('user').countDocuments().then((count) => {
                var number = between(100, 999);
                var inviterCode = `${number}${count}`
                insertJson.inviterCode = inviterCode;

                //check if email exists
                db.collection('user').find(
                    {
                        'email': email
                    })
                    .toArray()
                    .then(respostacoiso => {
                        if (respostacoiso.length != 0) {
                            response.json({
                                status: 300,
                                message: "Email already registered"
                            });
                            console.log("Email already registered")
                            return
                        }
                        db.collection('user').find(
                            {
                                'name': name
                            }
                        ).toArray()
                            .then(responsi => {
                                console.log(responsi)
                                if (responsi.length != 0) {
                                    response.json({
                                        status: 400,
                                        message: "Name already registered"
                                    });
                                    console.log('Name already registered');
                                } else {
                                    console.log("JUST STOP")
                                    //Insert data

                                    if (inviteCode != '') {
                                        db.collection('user')
                                            .find({ 'inviterCode': inviteCode })
                                            .toArray()
                                            .then(function (aResposta) {
                                                if (aResposta.length == 0) {
                                                    response.json({
                                                        status: 250,
                                                        message: 'Wrong invite code'
                                                    })
                                                    return;
                                                }

                                                var user = aResposta[0];
                                                var userId = user._id;
                                                var XP = user.XP;

                                                XP = XP + 50;

                                                var updateJson = {
                                                    'XP': XP
                                                }

                                                db.collection('user').updateOne({ '_id': objectID(userId) },
                                                    { $set: updateJson })

                                                db.collection('user').insertOne(insertJson, function (error, res) {
                                                    response.json({
                                                        status: 200,
                                                        message: "Registered successefully"
                                                    });
                                                    console.log('Registered successfully');
                                                })
                                            })
                                    }
                                }
                            })
                    })
                    .catch(errori => {

                    })
            });

        });

        app.post('/login', (request, res, next) => {

            var postData = request.body;
            var email = postData.email;
            var userPswd = postData.pswd;

            var db = client.db('ProjetoIII');


            //check if email exists
            db.collection('user')
                .find({ 'email': email })
                .toArray()
                .then(function (respostinha) {
                    console.log(respostinha);
                    if (respostinha.length == 0) {
                        res.json({
                            status: 208,
                            message: {
                                message: 'Email not registered'
                            }
                        });
                        return;
                    }

                    var user = respostinha[0];

                    var salt = user.salt; //get salt from user
                    var hashedPswd = checkHashPswd(userPswd, salt).pswdHash; //hash pswd with salt
                    var encryptedPswd = user.pswd; //get pswd from user

                    delete user.pswd;
                    delete user.salt;
                    console.log(user)

                    if (hashedPswd == encryptedPswd) {
                        res.json({
                            status: 200,
                            message: {
                                user: user,
                            }
                        });
                        console.log('Loged in successfully');
                    } else {
                        res.json({
                            status: 207,
                            message: "Password errada"
                        });
                        console.log('Wrong credentials');
                    }

                }).catch(erroris => {
                    console.log(erroris);
                    res.json("Um erro");
                })

        });


        app.post('/addline', (request, response, next) => {

            var data = request.body;
            var num = data.num;
            var user = data.user;
            var coords = data.coords.split(";");
            // var coord2 = data.coord2.split(";");
            // let coord3 = data.coord3.split(";");
            // var coord4 = data.coord4.split(";");
            var full = Boolean(data.full);
            var count = parseFloat(data.count);

            // data.coord = "24.5;78.2"
            // data.coord.split(";") === [24.5, 78.9]

            let coordinates = [];
            coordinates.push({
                latitude: parseFloat(coords[0]),
                longitude: parseFloat(coords[1])
            });
            coordinates.push({
                latitude: parseFloat(coords[2]),
                longitude: parseFloat(coords[3])
            });
            coordinates.push({
                latitude: parseFloat(coords[4]),
                longitude: parseFloat(coords[5])
            });
            coordinates.push({
                latitude: parseFloat(coords[6]),
                longitude: parseFloat(coords[7])
            });

            let usersConf = [];

            usersConf.push(user);

            var insertJson = {
                'num': num,
                'user': user,
                'coordinates': coordinates,
                'full': full,
                'count': count,
                'usersConf': usersConf
            }

            console.log(data);

            var db = client.db('ProjetoIII');

            db.collection('fila').insertOne(insertJson, (error, res) => {
                response.json('lines added c:');
                console.log('lines added c:');
            })

        });

        app.post('/addcurve', (request, response, next) => {

            var data = request.body;
            var num = data.num;
            var user = data.user;
            var coords = data.coords.split(";");
            var full = Boolean(data.full);
            var count = parseFloat(data.count);

            let coordinates = [];
            coordinates.push({
                latitude: parseFloat(coords[0]),
                longitude: parseFloat(coords[1])
            });
            coordinates.push({
                latitude: parseFloat(coords[2]),
                longitude: parseFloat(coords[3])
            });
            coordinates.push({
                latitude: parseFloat(coords[4]),
                longitude: parseFloat(coords[5])
            });
            coordinates.push({
                latitude: parseFloat(coords[6]),
                longitude: parseFloat(coords[7])
            });
            coordinates.push({
                latitude: parseFloat(coords[8]),
                longitude: parseFloat(coords[9])
            });
            coordinates.push({
                latitude: parseFloat(coords[10]),
                longitude: parseFloat(coords[11])
            });
            coordinates.push({
                latitude: parseFloat(coords[12]),
                longitude: parseFloat(coords[13])
            });
            coordinates.push({
                latitude: parseFloat(coords[14]),
                longitude: parseFloat(coords[15])
            });

            let usersConf = [];

            usersConf.push(user);

            var insertJson = {
                'num': num,
                'user': user,
                'coordinates': coordinates,
                'full': full,
                'count': count,
                'usersConf': usersConf
            }

            console.log(data);

            var db = client.db('ProjetoIII');

            db.collection('fila').insertOne(insertJson, (error, res) => {
                response.json('curves added c:');
                console.log('curves added c:');
            })

        });

        app.get('/getlines', (request, response) => {

            var db = client.db('ProjetoIII');

            db.collection('fila').find({}).toArray().then(resposta => {
                if (resposta.length == 0) {
                    response.json("No parking line to show");
                    console.log("No parking line to show");
                } else {
                    response.json(resposta);
                }

            }).catch(erro => {
                console.log(erro);
            })
        })

        app.post('/deleteline', (request, response, next) => {

            var postData = request.body;
            var id = postData.id;

            var db = client.db('ProjetoIII');

            db.collection('fila').deleteOne({ "_id": new mongodb.ObjectId(id) }).then(resposta => {
                if (resposta.length == 0) {
                    response.json("erro");
                    console.log("erro");
                } else {
                    response.json(resposta);
                }

            }).catch(erro => {
                console.log(erro);
            })

        })

        app.post('/editline', (request, response, next) => {

            var data = request.body;
            var id = data.id;
            var user = data.user;
            var full = Boolean(data.full);
            var count = parseFloat(data.count);

            var db = client.db('ProjetoIII');

            db.collection('fila').updateOne({ "_id": new mongodb.ObjectId(id) },
                { $set: { "user": user, "full": full, "count": count } }).then(resposta => {
                    if (resposta.length == 0) {
                        response.json("erro");
                        console.log("erro");
                    } else {
                        response.json(resposta);
                        console.log(resposta.result);
                    }

                }).catch(erro => {
                    console.log(erro);
                })

            if (count != 0) {
                db.collection('fila').updateOne({ "_id": new mongodb.ObjectId(id) },
                    { $push: { "usersConf": user } }).then(resposta => {
                        if (resposta.length == 0) {
                            response.json("erro");
                            console.log("erro");
                        } else {
                            response.json(resposta);
                            console.log(resposta.result);
                        }

                    }).catch(erro => {
                        console.log(erro);
                    })
            } else {
                db.collection('fila').updateOne({ "_id": new mongodb.ObjectId(id) },
                    { $set: { "usersConf": [user] } }).then(resposta => {
                        if (resposta.length == 0) {
                            response.json("erro");
                            console.log("erro");
                        } else {
                            response.json(resposta);
                            console.log(resposta.result);
                        }

                    }).catch(erro => {
                        console.log(erro);
                    })
            }

        });

        app.post('/getspecificline', (request, response) => {

            var data = request.body;

            var id = data.id;

            var db = client.db('ProjetoIII');

            db.collection('fila').findOne({ "_id": new mongodb.ObjectId(id) }).then(resposta => {
                console.log(resposta); //dá array vazio :c
                if (resposta.length == 0) {
                    response.json("No parking line to show");
                    console.log("No parking line to show");
                } else {
                    response.json(resposta);
                }

            }).catch(erro => {
                console.log(erro);
            })
        })


        app.listen(3000, () => {
            console.log('Connected to DB server');
        })
    }

    app.post('/alterUser', (request, res, next) => {

        var postData = request.body;
        var id = postData.id;
        var passwordN = postData.passwordN;
        var userPswd = postData.pswd;

        var db = client.db('ProjetoIII');

        //check if id exists
        db.collection('user')
            .find({ '_id': objectID(id) })
            .toArray()
            .then(function (respostinha) {
                console.log(respostinha);
                if (respostinha.length == 0) {
                    res.json('id not registered');
                    return;
                }

                var user = respostinha[0];

                var userId = user._id;
                var salt = user.salt; //get salt from user

                var hashedPswd = checkHashPswd(userPswd, salt).pswdHash; //hash pswd with salt
                var encryptedPswd = user.pswd; //get pswd from user
                var hashData = saltHashPswd(passwordN);

                var pswd = hashData.pswdHash; //save pswd hash
                var saltM = hashData.salt; //save salt

                var updateJson = {
                    'pswd': pswd,
                    'salt': saltM,
                };

                if (hashedPswd == encryptedPswd) {
                    db.collection('user').updateOne({ '_id': objectID(userId) },
                        { $set: updateJson }
                    )
                    res.json({
                        status: true,
                        message: 'Password updated successfully'
                    });
                    console.log('Sucesso');
                } else {
                    res.json({
                        status: false,
                        message: "Password does not match"
                    });
                    console.log('Password does not match');
                }

            }).catch(erroris => {
                console.log(erroris);
                res.json("Um erro");
            })

    });
});

